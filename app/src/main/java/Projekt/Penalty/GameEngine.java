package Projekt.Penalty;

import java.util.Arrays;
import java.util.Random;

public class GameEngine implements Penalty {

    Goalkeeper goalkeeper = new Goalkeeper();
    Shooter shooter = new Shooter();
    private final League league = new League();

    private Team myTeam, enemyTeam, activeTeam;

    public void setLeague(Team[] league) {
        this.league.setLeague(league);
    }

    public Team[] getLeague() {
        return this.league.getLeague();
    }

    private int[] goal = new int[6];

    private boolean playOff, gameOver;

    private int currentPlayer = shooter.getValue();
    private int enemy = goalkeeper.getValue();
    private int score1, score2, round, generalRound, changedPlayer; //round: prüft auf 5 Schüsse pro Runde ;; generalRound: Sorgt dafür das 2x 5 Schüsse gemacht werden

    public void setMyTeam(Team in) {
        assert in != null : "must enter a valid Team";
        myTeam = in;
        activeTeam = in;
    }

    public Team getMyTeam() {
        return this.myTeam;
    }

    public Team getEnemyTeam() {
        return this.enemyTeam;
    }

    public void setEnemyTeam(Team in) {
        enemyTeam = in;
    }

    public int getCurrentPlayer() {
        return this.currentPlayer;
    }

    public int getShootPos() {
        return shooter.getPosition();
    }

    public int getScore1() {
        return this.score1;
    }

    public int[] getGoal() {
        return goal;
    }

    public int getScore2() {
        return this.score2;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public void setShot(int pos) {
        assert pos < 6 : "Entry is out of range";
        this.goal[pos] += currentPlayer;
    }

    public void setGoalkeeper(int pos) {
        assert pos < 7 : "invalid Position";                        //die Zahl 6 ist absichtlich mit eingebunden als default-Wert bei einem ins Aus klicken
        this.goal[pos] += enemy;
    }

    public int getAngle() {
        return goalkeeper.getAngle();
    }

    public void setPosition(int pos) {
        assert pos < 7 : "position out of range";
        if (currentPlayer == 1) {
            shooter.setPosition(pos);
        } else {
            goalkeeper.setPosition(pos);
            setGoalkeeper(pos);
        }
    }

    //Ist für die Zufalls-Entscheidung des Gegners zuständig
    public void setRandomPos() {
        int pos = randomizer();
        if (currentPlayer == 1) {
            goalkeeper.setPosition(pos);
            setGoalkeeper(pos);
        } else {
            shooter.setPosition(pos);
        }
    }

    //Methode wird aufgerufen, wenn Nutzer ins Aus klickt, also außerhalb des Wertebereichs
    public void outClicked() {
        shooter.setPosition(currentPlayer == shooter.getValue() ? 6 : 2);
        goalkeeper.setPosition(1);
    }

    //Methode gibt true zurück wenn aktuelle Position vom Schuss nicht vom Torhüter gedeckt wurde
    public boolean checkGoal() {
        return Arrays.stream(goal).anyMatch(n -> n != 0);
    }

    public void checkGameStatus() {

        //Falls wir in der Playoff-Runde sind muss nach jedem Schuss gewechselt werden
        if (playOff) {
            changePlayer();
            changedPlayer++;
        } else {
            //Wechselt Spieler nach 5 Schüssen
            if (round == 5 || round == 10) {
                generalRound++;
                changePlayer();
            }
            //Prüft, ob nach 10 Schüssen ein Gewinner steht oder unentschieden ist
            if (generalRound == 2 && round % 2 == 0) {
                if (checkWinner()) {
                    gameOver = true;
                } else {
                    playOff = true;
                }
            }
            //Runde wid erhöht
            round++;
        }

        //prüft in der Playoff-Runde nach 2 Schüssen ob es ein Gewinner gibt
        if (changedPlayer % 2 == 0 && changedPlayer != 0) {
            if (checkWinner()) gameOver = true;
        }
    }

    //Der Score wird erhöht jeweils für oder gegen den Spieler
    public void entryScore() {
        if (checkGoal()) {
            if (currentPlayer == 1) {
                score1++;
            } else {
                score2++;
            }
        } else {
            if (currentPlayer == 1) {
                score2++;
            } else {
                score1++;
            }
        }
    }

    public void updateScore() {
        if (shooter.getPosition() != 6) {
            setShot(shooter.getPosition());
            entryScore();
        } else {
            if (currentPlayer == 1) {
                if (activeTeam.equals(myTeam)) {
                    score2++;
                } else {
                    score1++;
                }
            } else {
                if (activeTeam.equals(enemyTeam)) {
                    score2++;
                } else {
                    score1++;
                }
            }
        }
    }

    //Methode gibt true wenn es einen Gewinner gibt
    public boolean checkWinner() {
        return score1 > score2 || score2 > score1;
    }

    //aktueller Spieler wird gewechselt -> entweder jeweils auf Torwart oder Schütze
    public void changePlayer() {
        assert currentPlayer + enemy == 0 : "One Person is out of Range";
        int switcher = currentPlayer;
        currentPlayer = enemy;
        enemy = switcher;
        if (activeTeam.equals(myTeam)) {
            activeTeam = enemyTeam;
        }
        else{
            activeTeam = myTeam;
        }
        assert currentPlayer != enemy : "Enemy didnt switch";
    }

    public void freshGoal() {
        assert goal != null : "goal does not exist";
        goal = new int[6];
    }

    //gibt eine Zufallszahl von 0-5
    public int randomizer() {
        Random r = new Random();
        return r.nextInt(6);
    }

    //Zurücksetzen aller Statistiken
    public void resetStats() {
        gameOver = false;
        playOff = false;
        changedPlayer = 0;
        this.currentPlayer = 1;
        this.enemy = -1;
        this.round = 0;
        this.generalRound = 0;
        this.score1 = 0;
        this.score2 = 0;
    }

}

