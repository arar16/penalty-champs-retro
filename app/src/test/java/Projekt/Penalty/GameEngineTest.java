package Projekt.Penalty;


import org.junit.jupiter.api.*;

import java.util.Arrays;


public class GameEngineTest {

    GameEngine testedObj = new GameEngine();

    @Test
    @Order(1)
    void startTests() {
        try {
            assert false;
        } catch (AssertionError e) {
            System.out.println("\nCool, tests getting started!\n");
        }
    }

    @Test
    @DisplayName("SetShot-Method tests")
    @Order(2)
    void testSetShot() {
        GameEngine tempObj = new GameEngine();

        //Does setShot work?
        testedObj.setShot(5);
        Assertions.assertArrayEquals(new int[]{0, 0, 0, 0, 0, 1}, testedObj.getGoal(), "setting Shot just failed");
        tempObj.setShot(5);
        Assertions.assertArrayEquals(testedObj.getGoal(), tempObj.getGoal());

        //testing refreshing goal
        testedObj.freshGoal();
        Assertions.assertArrayEquals(new int[6], testedObj.getGoal(), "refreshing goal just failed");

        //is setShot secured of invalid operations
        try {
            testedObj.setShot(7);
        } catch (AssertionError ai) {
            System.out.print("Code ... ");
            testedObj.setShot(1);
        }
        Assertions.assertArrayEquals(new int[]{0, 1, 0, 0, 0, 0}, testedObj.getGoal(), "setShot didnt give AssertionError");
    }

    @Test
    @DisplayName("Goalkeeper-Method tests")
    void testSetGoalkeeper() {
        //Setting Goalkeeper tests
        testedObj.setGoalkeeper(3);
        Assertions.assertArrayEquals(testedObj.getGoal(), new int[]{0, 0, 0, -1, 0, 0}, "setting Goalkeeper just failed");

        //invalid Operation with Method must exit AssertionError
        try {
            testedObj.setShot(7);
        } catch (AssertionError ae) {
            testedObj.setGoalkeeper(3);
        }
        Assertions.assertNotEquals(testedObj.getGoal(), new int[]{0, 0, 0, -2, 0, 0}, "setShot didnt give Error");
    }

    @Test
    @DisplayName("CheckGoal-Method tests")
    void testCheckGoal() {
        GameEngine tmpObj = new GameEngine();

        //checkGoal on empty Goal
        if (testedObj.checkGoal()) {
            Assertions.assertEquals(1, 2, "checkGoal should have been false");
        }

        //Basic operation: Shot --> Goal and Shot --> Parried: Does the checkGoal-Method work right?
        testedObj.setShot(2);
        Assertions.assertTrue(testedObj.checkGoal(), "testCheckGoal at Shot failed");
        testedObj.setGoalkeeper(2);
        Assertions.assertFalse(testedObj.checkGoal(), "testCheckGoal at parry Goal failed");
        tmpObj.setShot(2);
        testedObj.setShot(2);
        Assertions.assertEquals(testedObj.checkGoal(), tmpObj.checkGoal(), "testCheckGoal failed");
    }

    @Test
    @DisplayName("UpdateScore-Method tests")
    void testUpdateScore() {
        testedObj.resetStats();
        testedObj.setMyTeam(Teams.FENERBAHCE);

        //updateScore after Goal
        testedObj.setGoalkeeper(4);
        testedObj.setPosition(2);
        testedObj.updateScore();
        Assertions.assertEquals(1, testedObj.getScore1(), "Score didnt update right at scoring goal");

        //update Score after goal saved
        testedObj.freshGoal();
        testedObj.setGoalkeeper(2);
        testedObj.updateScore();
        Assertions.assertEquals(1, testedObj.getScore2(), "Score didnt update right although Goalkeeper saves the Ball");

        //updating score after changing Player
        testedObj.freshGoal();
        testedObj.setPosition(5);           // Position of Shooter
        testedObj.changePlayer();          //Player switches from shooter to Goalie
        testedObj.setPosition(5);         //Position of Goalie
        testedObj.updateScore();
        Assertions.assertEquals(2, testedObj.getScore1(), "Score didnt update right after changing current layer");
    }

    @Test
    @DisplayName("ChangePlayer-Method tests")
    void changePlayerTests() {
        //After changing Player currentPlayer must equal the Goalkeeper value
        testedObj.changePlayer();
        Assertions.assertEquals(testedObj.goalkeeper.getValue(), testedObj.getCurrentPlayer(), "changePlayer didnt work");
    }

    @Test
    @DisplayName("Getter und Setter")
    void getSetMethods() {
        testedObj.setMyTeam(Teams.FENERBAHCE);

        //setting simple Position for shooting
        testedObj.setPosition(3);
        Assertions.assertEquals(testedObj.getShootPos(), 3, "setting shoot Position failed");

        //testing if setPosition in GameEngine makes changes in Shooter class
        Assertions.assertEquals(testedObj.getShootPos(), testedObj.shooter.getPosition(), "setPosition didnt associate with shooter");

        //Is the Method secured of invalid operations?
        try {
            testedObj.changePlayer();
            testedObj.setPosition(7);
        } catch (AssertionError ae) {
            testedObj.resetStats();
        }
        //if this Assertion gives an error --> catch didnt come into use --> AssertionError not thrown
        Assertions.assertEquals(testedObj.getCurrentPlayer(), 1, "setShoot Method exception didnt func");

        testedObj.freshGoal();

        //testing if setPosition works on goalkeeper class and does it work the right way
        testedObj.changePlayer();
        testedObj.setPosition(4);
        Assertions.assertEquals(testedObj.goalkeeper.getAngle(), testedObj.getAngle(), "getAngle is set wrong");
        testedObj.goalkeeper.setPosition(3);
        Assertions.assertEquals(275, testedObj.getAngle(), "getAngle is set wrong");


        //testing if setRandomPos really sets a random position
        testedObj.freshGoal();
        testedObj.changePlayer();
        testedObj.setRandomPos();
        Assertions.assertTrue(Arrays.stream(testedObj.getGoal()).anyMatch(n -> n != 0), "no random Position set!");
    }

    @Test
    @DisplayName("outClicked-Method")
    void outClickedTests() {
        //Does outClicked work like we want
        testedObj.setMyTeam(Teams.FENERBAHCE);
        testedObj.outClicked();
        Assertions.assertEquals(6, testedObj.getShootPos(), "shooter Position not right set after out clicking");
        testedObj.updateScore();
        Assertions.assertEquals(1, testedObj.getScore2(), "outClicked didnt made that the opponent becomes the score ");
    }

    @Test
    @DisplayName("checkGameStatus-Method")
    void checkGameStatusTests() {
        testedObj.setMyTeam(Teams.FENERBAHCE);

        //here should nothing switch
        testedObj.checkGameStatus();
        Assertions.assertEquals(1, testedObj.getCurrentPlayer(), "checkGameStatus failed");

        //After 5 shots the currentPlayer has to switch in this method
        testedObj.checkGameStatus();
        testedObj.checkGameStatus();
        testedObj.checkGameStatus();
        testedObj.checkGameStatus();
        testedObj.checkGameStatus();
        Assertions.assertEquals(-1, testedObj.getCurrentPlayer(), "checkGameStatus switch failed");
    }

    @Test
    @DisplayName("entryScore-Method")
    void entryScoreTests() {
        testedObj.setMyTeam(Teams.FENERBAHCE);

        //Basic operation setting shot --> goal --> entering score right?
        testedObj.setPosition(2);
        testedObj.setShot(2);
        testedObj.entryScore();
        Assertions.assertEquals(1, testedObj.getScore1(), "entryScore failed");

        //How does entryScore work at parrying goal?
        testedObj.setGoalkeeper(2);
        testedObj.entryScore();
        Assertions.assertEquals(1, testedObj.getScore2(), "entry enemy point failed ");
        Assertions.assertNotEquals(2, testedObj.getScore1(), "entries wrong score");

        //Does it work for the other case too?
        testedObj.setMyTeam(Teams.GALATASARAY);
        testedObj.freshGoal();
        testedObj.setPosition(4);
        testedObj.setShot(4);
        testedObj.setGoalkeeper(4);
        testedObj.entryScore();
        Assertions.assertEquals(2, testedObj.getScore1(), "Entering wrong Team-score");
    }

    @Test
    @DisplayName("checkWinner")
    void checkWinnerTest() {
        testedObj.setMyTeam(Teams.FENERBAHCE);
        Assertions.assertFalse(testedObj.checkWinner(), "checkWinner gives a winner without scoring");

        testedObj.setPosition(2);
        testedObj.setShot(2);
        testedObj.setGoalkeeper(4);
        testedObj.entryScore();
        Assertions.assertTrue(testedObj.checkWinner(), "checkWinner dont work right");
    }

    @Test
    @DisplayName("isGameOver")
    void isGameOverTests() {
        testedObj.setMyTeam(Teams.FENERBAHCE);

        testedObj.setPosition(4);
        testedObj.setGoalkeeper(1);
        Assertions.assertFalse(testedObj.isGameOver(), "Game is over, if 10 times shot");

        for (int i = 0; i < 11; i++) {
            testedObj.updateScore();
            //round just getting updated 10 times
            testedObj.checkGameStatus();
        }
        Assertions.assertTrue(testedObj.isGameOver(), "After 10 Shots match must be over");
    }

    @Test
    @DisplayName("last Tests")
    void extraTests() {
        testedObj.setMyTeam(Teams.FENERBAHCE);
        int test = testedObj.getCurrentPlayer();
        testedObj.changePlayer();
        testedObj.changePlayer();
        Assertions.assertEquals(test, testedObj.getCurrentPlayer());

        //testing resetStats
        testedObj.setPosition(2);
        testedObj.setGoalkeeper(3);
        testedObj.updateScore();
        testedObj.changePlayer();
        testedObj.resetStats();
        Assertions.assertEquals(testedObj.shooter.getValue(), testedObj.getCurrentPlayer(), "resetStats doesnt reset CurrentPlayer");
        Assertions.assertEquals(0, testedObj.getScore1(), "resetStats doesnt reset scores");

        testedObj.changePlayer();
        testedObj.setShot(2);
        testedObj.setGoalkeeper(2);
        testedObj.entryScore();
        Assertions.assertEquals(1, testedObj.getScore2());
        Assertions.assertEquals(-1, testedObj.getCurrentPlayer());
    }

    @AfterAll
    public static void output() {
        System.out.print("processed!\nCongratulations! All tests passed!\n\n");
    }
}