package Projekt.Penalty;

import processing.core.PApplet;
import processing.core.PImage;

public class DI extends PApplet {

    Penalty ge = new GameEngine();


    PImage net, ball, goalie, saveGoalie, fb, gs,bjk,ts,ads,alanya,antalya,gfk, giresun, goeztepe, hatay, basaksehir, karaguemruk , kasimpasa, kayseri, konya, rize, sivas, malatya, altay, pitch, stadium, scoreboard, trophy;

    boolean setup, clubSelected, loopCancelerVar, firstClubSelected;
    int xBallSpeed = 450, yBallSpeed = 550;


    public static void main(String[] args) {
        DI x = new DI();
        x.runSketch(new String[]{});
    }

    //Fenstergröße
    public void settings() {
        size(900, 600);
    }

    //Bilder werden hochgeladen
    public void setup() {
        rectMode(CENTER);
        ellipseMode(CENTER);
        imageMode(CENTER);
        textAlign(CENTER);
        surface.setTitle("Penalty Champs Retro");

        fb = loadImage("FB.png");
        gs = loadImage("GS.png");
        bjk = loadImage("Besiktas_JK.png");
        ts = loadImage("Trabzonspor.png");
        ads = loadImage("Adana_Demirspor.png");
        alanya = loadImage("Alanya_spor.png");
        antalya = loadImage("Antalyaspor.png");
        gfk = loadImage("Gaziantep_FK.png");
        giresun = loadImage("Giresunspor.png");
        goeztepe = loadImage("Goeztepe.png");
        hatay = loadImage("Hatayspor.png");
        basaksehir = loadImage("Istanbul_Basaksehir_FK_Logo.png");
        karaguemruk = loadImage("Karagumruk.png");
        kasimpasa = loadImage("Kasimpasa_Logo.png");
        kayseri = loadImage("Kayserispor_logosu.png");
        konya = loadImage("Konyaspor.png");
        rize = loadImage("Rizespor.png");
        sivas = loadImage("Sivasspor.png");
        malatya = loadImage("Yeni_Malatyaspor.png");
        altay = loadImage("Altay.png");
        trophy = loadImage("Trophy.png");
        scoreboard = loadImage("Scoreboard.png");
        stadium = loadImage("Stadium2.jpg");
        net = loadImage("net11.png");
        goalie = loadImage("Standing Goalie.png");
        saveGoalie = loadImage("Saving Goalie.png");
        ball = loadImage("ball2.png");
        pitch = loadImage("Pitch.jpg");
        Team fener = new Team(Teams.FENERBAHCE, fb);
        Team Gala = new Team(Teams.GALATASARAY, gs);
        Team Besiktas = new Team(Teams.BESIKTAS, bjk);
        Team Trabzon = new Team(Teams.TRABZONSPOR, ts);
        Team ADS = new Team(Teams.ADANASPOR, ads);
        Team Alanya = new Team(Teams.ALANYASPOR, alanya);
        Team Antalya = new Team(Teams.ANTALYASPOR, antalya);
        Team Gfk = new Team(Teams.GAZISEHIR, gfk);
        Team Giresun = new Team(Teams.GIRESUNSPOR, giresun);
        Team Goeztepe = new Team(Teams.GOEZTEPE, goeztepe);
        Team Hatay = new Team(Teams.HATAYSPOR, hatay);
        Team Basaksehir = new Team(Teams.BASAKSEHIR, basaksehir);
        Team Karaguemruek = new Team(Teams.KARAGUEMRUEK, karaguemruk);
        Team Kasimpasa = new Team(Teams.KASIMPASA, kasimpasa);
        Team Kayseri = new Team(Teams.KAYSERISPOR, kayseri);
        Team Konya = new Team(Teams.KONYASPOR, konya);
        Team Rize = new Team(Teams.RIZESPOR, rize);
        Team Sivas = new Team(Teams.SIVASSPOR, sivas);
        Team Malatya = new Team(Teams.MALATYASPOR, malatya);
        Team Altay = new Team(Teams.ALTAY_FK, altay);

        Team[] league = new Team[] {fener, Gala, Besiktas, Trabzon, Basaksehir, Konya, Rize, Goeztepe, Altay, Malatya, Kasimpasa, Hatay, Karaguemruek
        ,Antalya, Kayseri, ADS, Sivas, Gfk, Alanya, Giresun};

        ge.setLeague(league);
        //setField();
    }

    //Das Spielfeld wird eingerichtet und sorgt im Hintergrund für weiter anstehende Ambience
    private void setField() {
        image(stadium, 450, 250, 900, 600);
        image(net, 450, 250, 500, 300);
        image(pitch, 450, 550, 900, 300);
        stroke(240);
        line(0, 400, 900, 400);
        line(200, 100, 250, 200);
        line(700, 100, 650, 200);
        line(250, 200, 250, 400);
        line(650, 200, 650, 400);
        noFill();
        strokeWeight(6);
        rect(450, 250, 500, 300);
        fill(255);
        ellipse(450, 550, 20, 20);
        image(scoreboard, 450, 50, 300, 100);
        image(ge.getMyTeam().getLogo(), 350, 30, 50, 50);
        image(ge.getEnemyTeam().getLogo(), 550, 30, 50, 50);

        textSize(32);
        text(ge.getScore1() + " : " + ge.getScore2(), 450, 40);
        textSize(20);
        text(ge.getCurrentPlayer() == 1 ? "Shooter" : "Goalkeeper", 450, 70);

    }

    //Das Spielfeld wird nach dem Schuss zurückgesetzt: Ball auf Mittelpunkt / Torwart in Mitte
    public void resetField() {
        image(ball, xBallSpeed = 450, yBallSpeed = 550, 40, 40);
        image(goalie, 450, 305, 275, 276);
    }

    public void draw() {

        image(ge.getLeague()[0].getLogo(),200,150, 300,300);

        if (clubSelected) {
            if (ge.isGameOver()) {
                surface.setTitle("Winner!!!!");
                String winner = ge.getScore1() > ge.getScore2() ? ge.getMyTeam().getTeam().name() : ge.getEnemyTeam().getTeam().name();
                String message = "The Champions: " + winner;
                int color1 = winner.equals(ge.getMyTeam().getTeam().name()) ?
                        color(ge.getMyTeam().getTeam().getColor2().r, ge.getMyTeam().getTeam().getColor2().g, ge.getMyTeam().getTeam().getColor2().b) :
                        color(ge.getEnemyTeam().getTeam().getColor2().r, ge.getEnemyTeam().getTeam().getColor2().g, ge.getEnemyTeam().getTeam().getColor2().b);
                int color2 = winner.equals(ge.getMyTeam().getTeam().name()) ?
                        color(ge.getMyTeam().getTeam().getColor1().r, ge.getMyTeam().getTeam().getColor1().g, ge.getMyTeam().getTeam().getColor1().b) :
                        color(ge.getEnemyTeam().getTeam().getColor1().r, ge.getEnemyTeam().getTeam().getColor1().g, ge.getEnemyTeam().getTeam().getColor1().b);
                fill(color1);
                rect(width/4f, height/2f, width/2f, height);
                fill(color2);
                rect(3*(width/4f), height/2f, width/2f, height);
                image(trophy, 160, 300, 200, 220);
                image(trophy, 740, 300, 200, 220);
                fill(169,169,169);
                textSize(48);
                text(message, width / 2f, 75);
                textSize(22);
                text("to play again: press any key", 200, 540);
                text("to Quit: press 'ENTER'", 200, 570);

                if (ge.getScore1() > ge.getScore2()) {
                    image(ge.getMyTeam().getLogo(), 450, 300, 350, 350);
                } else {
                    image(ge.getEnemyTeam().getLogo(), 450, 325, 350, 425);
                }
                if (keyPressed) {
                    if (key == ENTER) {
                        System.exit(0);
                    } else {
                        resetStats();
                        setup();
                    }
                }
            } else {
                setField();
                if (mouseButton == LEFT && !loopCancelerVar) {
                    saveGoal(ge.getAngle());
                    shoot(ge.getShootPos());
                } else {
                    resetField();
                }
            }
        } else {
            background(112,128,144);
            int x1 = 50, y1 = 150;
            for(int i = 0, j = 10; i < 6 || j < 16; i++, j++) {
                image(ge.getLeague()[i].getLogo(),x1,50,50,50);
                image(ge.getLeague()[j].getLogo(),x1,550,50,50);
                x1+= 160;
            }

            for(int i = 6, j = 16; i < 10 || j < 20; i++, j++) {
                image(ge.getLeague()[i].getLogo(),850,y1,50,50);
                image(ge.getLeague()[j].getLogo(),50,y1,50,50);
                y1+= 100;
            }

            fill(255,0,0);
            textSize(18);
            text("Choose one Team you wanna play then press 'TAB' and choose your opponent and press 'ENTER'\n",width/2f, 100);
            fill(255);
            textSize(20);
            text(
            """
                    Press 'F' for Fenerbahce
                    'G' for Galatasaray
                    'B' for Besiktas
                    'T' for Trabzon
                    'i' for Basaksehir
                    'K' for Konya
                    'R' for Rize
                    'O' Goeztepe
                    'L' for Altay
                    'M' for Malatya""",300,150);
        text("""
                    'P' for Kasimpasa
                    'H' for Hatay
                    'U' for Karaguemruek
                    'A' for Antalya
                    'Y' for Kayseri
                    'D' for Adana Demirspor
                    'S' for Sivasspor
                    'Z' for Gazisehir
                    'N' for Alanya
                    'Q' for Giresun""", 600, 150);

            if (keyPressed && !firstClubSelected){
                switch (key) {
                    case 'f', 'F' -> ge.setMyTeam(ge.getLeague()[0]);
                    case 'g', 'G' -> ge.setMyTeam(ge.getLeague()[1]);
                    case 'b', 'B' -> ge.setMyTeam(ge.getLeague()[2]);
                    case 't', 'T' -> ge.setMyTeam(ge.getLeague()[3]);
                    case 'i', 'I' -> ge.setMyTeam(ge.getLeague()[4]);
                    case 'k', 'K' -> ge.setMyTeam(ge.getLeague()[5]);
                    case 'r', 'R' -> ge.setMyTeam(ge.getLeague()[6]);
                    case 'o', 'O' -> ge.setMyTeam(ge.getLeague()[7]);
                    case 'L', 'l' -> ge.setMyTeam(ge.getLeague()[8]);
                    case 'm', 'M' -> ge.setMyTeam(ge.getLeague()[9]);
                    case 'p', 'P' -> ge.setMyTeam(ge.getLeague()[10]);
                    case 'h', 'H' -> ge.setMyTeam(ge.getLeague()[11]);
                    case 'u', 'U' -> ge.setMyTeam(ge.getLeague()[12]);
                    case 'A', 'a' -> ge.setMyTeam(ge.getLeague()[13]);
                    case 'y', 'Y' -> ge.setMyTeam(ge.getLeague()[14]);
                    case 'D', 'd' -> ge.setMyTeam(ge.getLeague()[15]);
                    case 's', 'S' -> ge.setMyTeam(ge.getLeague()[16]);
                    case 'z', 'Z' -> ge.setMyTeam(ge.getLeague()[17]);
                    case 'n', 'N' -> ge.setMyTeam(ge.getLeague()[18]);
                    case 'Q', 'q' -> ge.setMyTeam(ge.getLeague()[19]);
                    case TAB      ->{
                        firstClubSelected = true;
                        setup = true;
                    }
                }
            }
            if (keyPressed && firstClubSelected){
                switch (key) {
                    case 'f', 'F' -> ge.setEnemyTeam(ge.getLeague()[0]);
                    case 'g', 'G' -> ge.setEnemyTeam(ge.getLeague()[1]);
                    case 'b', 'B' -> ge.setEnemyTeam(ge.getLeague()[2]);
                    case 't', 'T' -> ge.setEnemyTeam(ge.getLeague()[3]);
                    case 'i', 'I' -> ge.setEnemyTeam(ge.getLeague()[4]);
                    case 'k', 'K' -> ge.setEnemyTeam(ge.getLeague()[5]);
                    case 'r', 'R' -> ge.setEnemyTeam(ge.getLeague()[6]);
                    case 'o', 'O' -> ge.setEnemyTeam(ge.getLeague()[7]);
                    case 'L', 'l' -> ge.setEnemyTeam(ge.getLeague()[8]);
                    case 'm', 'M' -> ge.setEnemyTeam(ge.getLeague()[9]);
                    case 'p', 'P' -> ge.setEnemyTeam(ge.getLeague()[10]);
                    case 'h', 'H' -> ge.setEnemyTeam(ge.getLeague()[11]);
                    case 'u', 'U' -> ge.setEnemyTeam(ge.getLeague()[12]);
                    case 'A', 'a' -> ge.setEnemyTeam(ge.getLeague()[13]);
                    case 'y', 'Y' -> ge.setEnemyTeam(ge.getLeague()[14]);
                    case 'D', 'd' -> ge.setEnemyTeam(ge.getLeague()[15]);
                    case 's', 'S' -> ge.setEnemyTeam(ge.getLeague()[16]);
                    case 'z', 'Z' -> ge.setEnemyTeam(ge.getLeague()[17]);
                    case 'n', 'N' -> ge.setEnemyTeam(ge.getLeague()[18]);
                    case 'Q', 'q' -> ge.setEnemyTeam(ge.getLeague()[19]);
                    case ENTER      -> clubSelected = true;
                }
            }
        }
    }


    //Berechnet Koordinaten vom Ball wie weit es jeweils in der DI gehen soll
    public void ballMove(int pos) {
        switch (pos) {
            case 0 -> {
                if (xBallSpeed >= 270 && yBallSpeed >= 180) {
                    xBallSpeed -= 10;
                    yBallSpeed -= 20;
                } else {
                    loopCancelerVar = true;
                }
            }

            case 1 -> {
                if (yBallSpeed >= 180) {
                    yBallSpeed -= 15;
                } else {
                    loopCancelerVar = true;
                }
            }

            case 2 -> {
                if (xBallSpeed <= 620 && yBallSpeed >= 180) {
                    xBallSpeed += 10;
                    yBallSpeed -= 20;
                } else {
                    loopCancelerVar = true;
                }
            }

            case 3 -> {
                if (xBallSpeed >= 260 && yBallSpeed >= 100) {
                    xBallSpeed -= 15;
                    yBallSpeed -= 15;
                } else {
                    loopCancelerVar = true;
                }
            }

            case 4 -> {
                if (yBallSpeed >= 320) {
                    yBallSpeed -= 20;
                    xBallSpeed -= 5;
                } else {
                    loopCancelerVar = true;
                }
            }

            case 5 -> {
                if (xBallSpeed <= 635 && yBallSpeed >= 320) {
                    xBallSpeed += 15;
                    yBallSpeed -= 15;
                } else {
                    loopCancelerVar = true;
                }
            }

            default -> {
                if (xBallSpeed >= 100 && yBallSpeed >= 360) {
                    xBallSpeed -= 15;
                    yBallSpeed -= 10;
                } else {
                    loopCancelerVar = true;
                }
            }
        }
    }

    //Die Interaktionsmethode -> unten mehr Details
    public void mouseClicked() {
        loopCancelerVar = false;

        if (clubSelected) {

            ge.checkGameStatus();

            ge.setRandomPos();

            //Bei Maus-Klick wird jeweils Schuss gesetzt in AL und DI
            if (mouseX > 200 && mouseX <= 350) {
                if (mouseY >= 100 && mouseY <= 250) {
                    ge.setPosition(0);
                } else if (mouseY > 250 && mouseY <= 400) {
                    ge.setPosition(3);
                } else {
                    ge.outClicked();
                }
            } else if (mouseX > 350 && mouseX <= 550) {
                if (mouseY > 0 && mouseY <= 250) {
                    ge.setPosition(1);
                } else if (mouseY > 250 && mouseY <= 400) {
                    ge.setPosition(4);
                } else {
                    ge.outClicked();
                }
            } else if (mouseX > 550 && mouseX <= 700) {
                if (mouseY > 0 && mouseY <= 250) {
                    ge.setPosition(2);
                } else if (mouseY > 250 && mouseY <= 400) {
                    ge.setPosition(5);
                } else {
                    ge.outClicked();
                }
            } else {
                ge.outClicked();
            }

            //Punkteaktualisierung -- Bei einem Schuss ins Aus kriegt der Gegner einen Punkt
            ge.updateScore();
            //Tor wird "leergeräumt" sodass Tor-Array wieder leer ist
            ge.freshGoal();
        }
    }

    //Die Animation des Balles wird hier dargestellt
    public void shoot(int pos) {
        ballMove(pos);
        image(ball, xBallSpeed, yBallSpeed, 40, 40);
    }

    //Rotation des Torwarts
    public void saveGoal(int in) {
        pushMatrix();
        if (in == 315 || in == 45) {
            translate(450, 350);
        } else translate(450, 400);
        rotate(radians(in));
        translate(-450, -400);
        image(saveGoalie, 450, 288, 140, 276);
        popMatrix();
    }

    //Bei einem Neustart müssen alle Statistiken zurückgesetzt
    public void resetStats() {
        loopCancelerVar = false;
        setup = false;
        clubSelected = false;
        xBallSpeed = 450;
        yBallSpeed = 550;
        ge.resetStats();
    }
}