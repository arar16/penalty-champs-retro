# <span style="color: #34A09D">Penalty Champs Retro⚽(PiS, SoSe2021)</span>
Ein kleines Elfmeterschießen-Minispiel im Retro-Stil wie es Papa noch kennt

Autor: Ahmet-Rüchan Arican, 5341696
***
## Kurzbeschreibung

Das Spiel handelt hierbei um das klassische Elfmeterschießen, wie wir es im echten Fußball kennen. [Mehr Infos](https://de.wikipedia.org/wiki/Elfmeterschie%C3%9Fen "Wikipedia-Artikel") \
Nach der Wahl des Teams geht es auch schon los:\
Das Tor ist in 6 Flächen unterteilt: **links**, **rechts** und die **Mitte** jeweils **oben** und **unten**. In einer dieser Flächen schießen sie.
Der Torwart springt dabei in eine rein zufällig gewählte Position. Trifft der Nutzer, so bekommt er einen Punkt. Ansonsten
kriegt der Gegner einen.\
Nachdem der Spieler 5 Schüsse absolviert hat, wird gewechselt, d. h. nun ist der **Nutzer** der **Torwart** und der 
**Computer** der **Schütze**, ihre Rolle sehen sie auch im Scoreboard.\
Wenn nach diesen 5 Schüssen (also insgesamt 10) immer noch kein Sieger feststeht, wird in die Playoff-Runde 
gegangen. Das bedeutet der Nutzer wechselt immer ab, jeweils ein Schuss und anschließend einmal parieren. Somit 
wird weiter gemacht bis ein Sieger feststeht. (141 Wörter)
***
## Screenshot zum Projekt
(*Die rot gekennzeichneten Grenzen können minimal von der Realität abweichen*)
![Startbildschirm](app/src/main/resources/MainScreen.jpg)
![Spiel](app/src/main/resources/Screenshot%20Game2.jpg)
![Endbildschirm](app/src/main/resources/EndScreen.jpg)
***

## Bedienungshinweise
Zu aller erst wählt der Nutzer im Startbildschirm ein Team aus, indem er, wie es dort steht, eine der ausgewählten 
Tasten drückt. Anschließend drückt der Nutzer die *TAB*-Taste und wählt sein Gegner-Team aus. Danach bestätigt er seine 
Eingabe mit *ENTER* und der Spaß kann beginnen.

Nun muss der Spieler auf das Tor klicken und der Ball geht in die gewählte Richtung\
**!!Achtung: Nach einem Mal klicken kann der Schuss nicht zurückgezogen oder geändert werden!!** Falls sie die 
Maus trotzdem währen dem Schuss bewegen wird der Prozess übersprungen und sie sehen nur, ob sie einen Score 
bekommen haben oder nicht\
Falls der Nutzer gerade der Schütze ist und außerhalb dieses Bereichs, also ins Aus schießt, kriegt der Gegner einen 
Punkt. Falls er der Torwart ist und sie ins Aus klicken, dann bleibt der Torwart nur stehen und der Gegner führt sein 
Schuss durch.\
Das Tor ist wie in dem [Screenshot](#screenshot-zum-projekt) unterteilt.\
Am Ende des Spiels muss der Nutzer nun, wenn er nochmal spielen will, irgendeine beliebige Taste drücken
oder wenn er das Spiel beenden will die Taste *ENTER* drücken.
***
## Übersicht auf die Dateien und die Lines of Code
### Dateiübersicht

    \Readme.md
    \gradlew
    \gradlew.bat
    \settings.gradle
    \app\build.gradle
    \app\core.jar
    \app\src\main\java\Projekt\Penalty\DI.java
    \app\src\main\java\Projekt\Penalty\GameEngine.java
    \app\src\main\java\Projekt\Penalty\Goalkeeper.java
    \app\src\main\java\Projekt\Penalty\Penalty.java
    \app\src\main\java\Projekt\Penalty\Shooter.java
    \app\src\main\java\Projekt\Penalty\Team.java
    \app\src\main\resources\Trophy.png
    \app\src\main\resources\ball2.png
    app/src/main/resources/Adana_Demirspor.png
    app/src/main/resources/Alanya_spor.png
    app/src/main/resources/Altay.png
    app/src/main/resources/Antalyaspor.png
    app/src/main/resources/Besiktas_JK.png
    app/src/main/resources/Gaziantep_FK.png
    app/src/main/resources/Giresunspor.png
    app/src/main/resources/Goeztepe.png
    app/src/main/resources/Hatayspor.png
    app/src/main/resources/Karagumruk.png
    app/src/main/resources/Kasimpasa_Logo.png
    app/src/main/resources/Kayserispor_logosu.png
    app/src/main/resources/Konyaspor.png
    app/src/main/resources/Rizespor.png
    app/src/main/resources/Sivasspor.png
    app/src/main/resources/Trabzonspor.png
    app/src/main/resources/Yeni_Malatyaspor.png
    app/src/main/resources/Istanbul_Basaksehir_FK_Logo.png
    \app\src\main\resources\FB.png
    \app\src\main\resources\GS.png
    \app\src\main\resources\net11.png
    \app\src\main\resources\Pitch.jpg
    \app\src\main\resources\Saving Goalie.png
    \app\src\main\resources\Scoreboard.png
    \app\src\main\resources\Screenshot Game2.jpg
    \app\src\main\resources\Stadium2.jpg
    \app\src\main\resources\Standing Goalie.png
    \app\src\test\java\Projekt\Penalty\GameEngineTest.java
    \gradle\wrapper\gradle-wrapper.jar
    \gradle\wrapper\gradle-wrapper.properties

### Lines of Code
    -------------------------------------------------------------------------------
    Language                     files          blank        comment           code
    -------------------------------------------------------------------------------
    Java                             9            127             26            675
    -------------------------------------------------------------------------------
    SUM:                             9            127             26            675
    -------------------------------------------------------------------------------

## Quellen
(Alle im Stand von 29.06.2021.)
### Bilder
- https://www.pngkey.com/png/detail/2-26292_chain-link-fence-texture-png-seamless-transparent-chain.png
- https://pngtree.com/freepng/oblique-oblique-posture-dummy-limb_3921864.html 
- https://pngtree.com/freepng/dummy-limb-cartoon-cartoon-limb_3921866.html 
- https://png.pngtree.com/png-vector/20190912/ourmid/pngtree-modren-ball-icon-vector-png-image_1726872.jpg 
- https://upload.wikimedia.org/wikipedia/commons/f/f6/Galatasaray_Sports_Club_Logo.png
- https://upload.wikimedia.org/wikipedia/commons/f/f6/Galatasaray_Sports_Club_Logo.png 
- https://www.pngkey.com/png/full/811-8117820_trophy-transparent-background-trophy-png.png 
- https://banner2.cleanpng.com/20180601/qtl/kisspng-fenerbahe-s-k-fenerbahe-men-s-basketball-euro-fb-5b11829113ed76.5359636415278741930816.jpg 
- https://static1.s123-cdn-static-a.com/uploads/4379155/800_5f8afaae91012.png 
- https://image.freepik.com/free-photo/green-grass-field-background-soccer-football-sports-green-lawn-pattern-texture-background-close-up_64749-2270.jpg 

### Inspirationen und Inhalte
- https://processing.org/reference/loadImage_.html 
- https://www.youtube.com/watch?v=hsAiIytWf-I 
- https://stackoverflow.com/ 
- https://www.baeldung.com/
- https://www.w3schools.com/java/





