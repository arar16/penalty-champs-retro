package Projekt.Penalty;

public class Goalkeeper {

    private final int value = -1;
    private int position;
    private int angle;

    public void setPosition(int position) {
        this.position = position;
        switch (position) {
            case 0 -> angle = 315;
            case 1, 4 -> angle = 0;
            case 2 -> angle = (45);
            case 3 -> angle = (275);
            case 5 -> angle = (90);
        }
    }

    public int getValue() {
        return value;
    }

    public int getAngle() {
        return angle;
    }
}
