package Projekt.Penalty;

import processing.core.PImage;

public class Team extends PImage{
    private final Teams team;
    private final PImage logo;

    Team(Teams team, PImage logo) {
        this.team = team;
        this.logo = logo;
    }

    public PImage getLogo() {
        return this.logo;
    }

    public Teams getTeam() {
        return this.team;
    }

}
