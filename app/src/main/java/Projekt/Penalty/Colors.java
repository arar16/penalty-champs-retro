package Projekt.Penalty;

public enum Colors {
    BLACK(0,0,0),
    WHITE(255,255,255),
    YELLOW(255,255,0),
    DARKBLUE(0,0,128),
    RED(255,0,0),
    BLUE(72,118,255),
    GREEN(0,100,0),
    ORANGE(255,127,0),
    DARKRED(139,0,0);

    int r, g, b;
    Colors(int farbe1, int farbe2, int farbe3) {
        this.r = farbe1;
        this.g = farbe2;
        this.b = farbe3;
    }
}
