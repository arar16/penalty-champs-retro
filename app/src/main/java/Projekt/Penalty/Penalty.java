package Projekt.Penalty;

public interface Penalty {
    int getScore2();

    int getScore1();

    int getCurrentPlayer();

    int getShootPos();

    int getAngle();

    void setMyTeam(Team myTeam);

    Team getMyTeam();

    Team getEnemyTeam();

    void setLeague(Team[] league);

    Team[] getLeague();

    void setEnemyTeam(Team Enemy);

    void setRandomPos();

    void setPosition(int position);

    void checkGameStatus();

    boolean isGameOver();

    void outClicked();

    void updateScore();

    void freshGoal();

    void resetStats();
}
