package Projekt.Penalty;

public class Shooter {
    private final int value = 1;
    private int position;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getValue() {
        return value;
    }
}
