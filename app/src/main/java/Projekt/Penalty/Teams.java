package Projekt.Penalty;

public enum Teams {

    FENERBAHCE(Colors.DARKBLUE, Colors.YELLOW),
    GALATASARAY(Colors.RED, Colors.YELLOW),
    BESIKTAS(Colors.BLACK, Colors.WHITE),
    TRABZONSPOR(Colors.DARKRED, Colors.BLUE),
    BASAKSEHIR(Colors.DARKBLUE, Colors.ORANGE),
    KONYASPOR(Colors.GREEN, Colors.WHITE),
    RIZESPOR(Colors.BLUE, Colors.GREEN),
    GOEZTEPE(Colors.RED, Colors.YELLOW),
    ALTAY_FK(Colors.BLACK, Colors.WHITE),
    MALATYASPOR(Colors.BLACK, Colors.YELLOW),
    KASIMPASA(Colors.BLUE, Colors.WHITE),
    HATAYSPOR(Colors.DARKRED, Colors.WHITE),
    KARAGUEMRUEK(Colors.BLACK, Colors.RED),
    ANTALYASPOR(Colors.RED, Colors.WHITE),
    KAYSERISPOR(Colors.RED, Colors.YELLOW),
    ADANASPOR(Colors.DARKBLUE, Colors.WHITE),
    SIVASSPOR(Colors.WHITE, Colors.RED),
    GAZISEHIR(Colors.BLACK, Colors.RED),
    ALANYASPOR(Colors.GREEN, Colors.ORANGE),
    GIRESUNSPOR(Colors.WHITE, Colors.GREEN);




    private final Colors color1, color2;

    Teams(Colors color1, Colors color2) {
        this.color1 = color1;
        this.color2 = color2;
    }

    public Colors getColor1(){
        return this.color1;
    }

    public Colors getColor2() {
        return this.color2;
    }
}
